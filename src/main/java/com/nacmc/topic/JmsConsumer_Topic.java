package com.nacmc.topic;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.IOException;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-08-31 10:51
 */
public class JmsConsumer_Topic
{
    public static final String ACTIVEMQ_URL = "tcp://192.168.163.134:61616";
    public static final String TOPIC_NAME = "topic-hgq";

    public static void main(String[] args) throws JMSException, IOException
    {
        System.out.println("1号");
        //1.创建连接工厂，按照指定的url地址，采用默认用户和密码
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        //2.通过连接工厂，获得连接connection并启动访问
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        //3.创建会话session
        //两个参数，第一个事物/第二个叫签收
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        //4.创建目的地（具体是队列还是topic）
        Topic topic = session.createTopic(TOPIC_NAME);
        //5.创建消费者
        MessageConsumer messageConsumer = session.createConsumer(topic);
        //通过监听的方式来消费消息
        messageConsumer.setMessageListener((message) ->
        {
                if (message != null && message instanceof TextMessage)
                {
                    TextMessage textMessage = (TextMessage) message;
                    try {
                        System.out.println("*****消费者接收到消息：*****"+textMessage.getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }

        });
        //保证控制台不关闭
        System.in.read();
        messageConsumer.close();
        session.close();
        connection.close();
    }
}
